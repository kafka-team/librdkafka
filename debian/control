Source: librdkafka
Priority: optional
Maintainer: Faidon Liambotis <paravoid@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 pkgconf,
 libcurl4-openssl-dev,
 liblz4-dev,
 libsasl2-dev,
 libssl-dev,
 libzstd-dev,
 python3,
 rapidjson-dev,
 zlib1g-dev,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Section: libs
Homepage: https://github.com/confluentinc/librdkafka
Vcs-Git: https://salsa.debian.org/kafka-team/librdkafka.git
Vcs-Browser: https://salsa.debian.org/kafka-team/librdkafka

Package: librdkafka1
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: library implementing the Apache Kafka protocol
 librdkafka is a C library implementation of the Apache Kafka protocol,
 providing Producer, Consumer and Admin clients. It was designed with message
 delivery reliability and high performance in mind, current figures exceed 1
 million msgs/second for the producer and 3 million msgs/second for the
 consumer.
 .
 More information about Apache Kafka can be found at http://kafka.apache.org/
 .
 This package contains the C shared library.

Package: librdkafka++1
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: library implementing the Apache Kafka protocol (C++ bindings)
 librdkafka is a C library implementation of the Apache Kafka protocol,
 providing Producer, Consumer and Admin clients. It was designed with message
 delivery reliability and high performance in mind, current figures exceed 1
 million msgs/second for the producer and 3 million msgs/second for the
 consumer.
 .
 More information about Apache Kafka can be found at http://kafka.apache.org/
 .
 This package contains the C++ shared library.

Package: librdkafka-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 librdkafka++1 (= ${binary:Version}),
 librdkafka1 (= ${binary:Version}),
 libcurl4-openssl-dev,
 liblz4-dev,
 libsasl2-dev,
 libssl-dev,
 libzstd-dev,
 zlib1g-dev,
 ${misc:Depends}
Description: library implementing the Apache Kafka protocol (development headers)
 librdkafka is a C library implementation of the Apache Kafka protocol,
 providing Producer, Consumer and Admin clients. It was designed with message
 delivery reliability and high performance in mind, current figures exceed 1
 million msgs/second for the producer and 3 million msgs/second for the
 consumer.
 .
 More information about Apache Kafka can be found at http://kafka.apache.org/
 .
 This package contains the development headers.
